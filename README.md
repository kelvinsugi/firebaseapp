# README #

Just Clone this project and please download first the google-services.json (you could get it from google api web) and put it to your app folder. And you are ready to go.

### What is this repository for? ###

This repository just a sample app about issue management utilizing google firebase, 
file picker library (filepicker version 1.0.8) and picasso library to load pictures

### Contribution guidelines ###

You could contribute the code if you want to add some features, or if you have better method to apply.

### Who do I talk to? ###

* Please contact me via email sugiarto.kelvin@gmail.com