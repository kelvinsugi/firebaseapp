package com.totalbp.issuemanagement.mPicasso;

import android.content.Context;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.totalbp.issuemanagement.R;

/**
 * Created by 140030 on 01/02/2017.
 */

public class PicassoClient {
    public static void downloadImage(Context c, String url, ImageView img){
        if(url != null && url.length()>0){
            Picasso.with(c).load(url).resize(300,300).centerCrop().placeholder(R.drawable.placeholder_large).into(img);
        }
        else{
            Picasso.with(c).load(R.drawable.placeholder_large).resize(300,300).centerCrop().into(img);
        }
    }
}
