package com.totalbp.issuemanagement.mData;

/**
 * Created by 140030 on 01/02/2017.
 */

public class Issue {

    private String issueTxt;
    private String url;
    private String issueDate;
    private String Category;
    private String fileUrl;
    private String timeStamp;
    private String key;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Issue() {
    }

    public String getIssueTxt() {
        return issueTxt;
    }

    public void setIssueTxt(String issueTxt) {
        this.issueTxt = issueTxt;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCategory() {
        return Category;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
