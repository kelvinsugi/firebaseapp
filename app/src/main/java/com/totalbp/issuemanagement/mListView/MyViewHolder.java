package com.totalbp.issuemanagement.mListView;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalbp.issuemanagement.R;



/**
 * Created by 140030 on 01/02/2017.
 */

public class MyViewHolder  {
    TextView issueTxt;
    ImageView img;
    TextView dateTxt;
    TextView categoryTxt;
    TextView attachmentTxt;
    TextView keyTxt;
    Button btnDelete;


    public MyViewHolder(View itemView){

        issueTxt = (TextView) itemView.findViewById(R.id.issueTxt);
        img = (ImageView) itemView.findViewById(R.id.issueImage);
        dateTxt = (TextView) itemView.findViewById(R.id.dateTxt);
        categoryTxt = (TextView) itemView.findViewById(R.id.CategoryTxt);
        attachmentTxt = (TextView) itemView.findViewById(R.id.attachmentTxt);
        keyTxt = (TextView) itemView.findViewById(R.id.IssueKey);
        btnDelete = (Button) itemView.findViewById(R.id.btnDelete);
    }





}



