package com.totalbp.issuemanagement.mListView;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.totalbp.issuemanagement.R;
import com.totalbp.issuemanagement.mData.Issue;
import com.totalbp.issuemanagement.mPicasso.PicassoClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by 140030 on 01/02/2017.
 */

public class CustomAdapter extends BaseAdapter{

    Context c;
    ArrayList<Issue> issues;
    LayoutInflater inflater;
    TextView t2;
    Firebase firebase;
    final static String DB_URI = "https://researchapp-9ee34.firebaseio.com";

    public CustomAdapter(Context c,ArrayList<Issue> issues) {
        this.c = c;
        this.issues = issues;
    }

    @Override
    public int getCount() {
        return issues.size();
    }

    @Override
    public Object getItem(int position) {
        return issues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null){
            convertView=inflater.inflate(R.layout.model,parent,false);
        }

        final MyViewHolder holder = new MyViewHolder(convertView);

        //BIND DATA
        holder.issueTxt.setText(issues.get(position).getIssueTxt());
        holder.dateTxt.setText(issues.get(position).getIssueDate());
        PicassoClient.downloadImage(c,issues.get(position).getUrl(),holder.img);
        holder.categoryTxt.setText(issues.get(position).getCategory());
        holder.attachmentTxt.setText(issues.get(position).getFileUrl());
        holder.keyTxt.setText(issues.get(position).getKey());

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String key = holder.keyTxt.getText().toString();
                //Toast.makeText(v.getContext(),  holder.keyTxt.getText(), Toast.LENGTH_SHORT).show();
                Firebase.setAndroidContext(c);
                firebase = new Firebase(DB_URI);
                firebase.child("Issue").child(key).removeValue();
            }
        });

        holder.attachmentTxt .setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                Date now = new Date();
                String fileName = "issue_attachment"+formatter.format(now);
//                Toast.makeText(v.getContext(), holder.attachmentTxt.getText(), Toast.LENGTH_SHORT).show();
//                Intent docActivity = new Intent(v.getContext(), DocViewActivity.class);
//                docActivity.putExtra("docLink", holder.attachmentTxt.getText());
//                v.getContext().startActivity(docActivity);

                String DownloadUrl = holder.attachmentTxt.getText().toString();
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));
                request.setDescription("sample pdf file for testing");   //appears the same in Notification bar while downloading
                request.setTitle(fileName);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalFilesDir(v.getContext(),null, fileName);
                // get download service and enqueue file
                DownloadManager manager = (DownloadManager) v.getContext().getSystemService(Context.DOWNLOAD_SERVICE);
                manager.enqueue(request);
            }
        });

        return convertView;
    }
}
