package com.totalbp.issuemanagement;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.totalbp.issuemanagement.mFireBase.Fire;
import com.totalbp.issuemanagement.mUtils.CircleTransform;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static com.totalbp.issuemanagement.R.id.mImageView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        View.OnTouchListener,NavigationView.OnNavigationItemSelectedListener
{

    private static final String urlProfileImg = "https://firebasestorage.googleapis.com/v0/b/researchapp-9ee34.appspot.com/o/images%2FPP_KELVIN.JPG?alt=media&token=4b02d708-a91b-4f01-9427-bfbbcaf17b61";
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE  = 99;;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    final static String DB_URI = "https://researchapp-9ee34.firebaseio.com";
    private ArrayList<String> docPaths = new ArrayList<>();
    private ArrayList<Uri> uriArrayList =  new ArrayList<>();
    EditText issueEditText, urlEditText, dateEditText, fileUrlText;
    String urlString;
    String fileUrl;
    Spinner spinner;
    Button saveBtn;

    Fire fire;
    ListView lv;
    DatabaseReference db;
    String itemSpinner;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    String mCurrentPhotoPath;
    ImageView CameraimgView;
    Dialog d;
    Uri contentUri;
    Uri contentFileUri;
    Uri downloadUrl;
    Uri fileDownloadUrl;
    TextView emailProfile;
    ImageView imgProfile;
    private View navHeader;
    private NavigationView navigationView;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Toast.makeText(v.getContext(), "heya", Toast.LENGTH_LONG).show();
        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.lv);
        db = FirebaseDatabase.getInstance().getReference();

//
//        tv1 = (TextView) findViewById(R.id.attachmentTxt);
//        tv1.setOnTouchListener(this);


        fire = new Fire(this,DB_URI,lv);

        fire.refershData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                displayDialog();
//                dispatchTakePictureIntent();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        imgProfile = (ImageView) navHeader.findViewById(R.id.imageProfile);
        emailProfile = (TextView) navHeader.findViewById(R.id.emailProfileTxt);

        loadNavHeader();



    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
//        txtName.setText("Ravi Tamada");
//        txtWebsite.setText("www.androidhive.info");

        Picasso.with(getApplicationContext()).load(urlProfileImg).transform(new CircleTransform())
                .resize(50,50).centerCrop()
                .placeholder(R.drawable.placeholder_large)
                .error(R.drawable.placeholder_large).into(imgProfile);

        // showing dot next to notifications label
        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        itemSpinner = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        //Toast.makeText(parent.getContext(), "Selected: " + itemSpinner, Toast.LENGTH_LONG).show();
    }

    //SHOW DIALOG
    private void displayDialog(){
        d = new Dialog(this);
        d.setTitle("Save Data");
        d.setContentView(R.layout.dialoglayout);
        myCalendar = Calendar.getInstance();


        issueEditText = (EditText) d.findViewById(R.id.nameEditText);
        urlEditText = (EditText) d.findViewById(R.id.urlEditText);
        saveBtn = (Button) d.findViewById(R.id.saveBtn);
        dateEditText = (EditText) d.findViewById(R.id.dateEditText);
        CameraimgView = (ImageView) d.findViewById(mImageView);
        fileUrlText = (EditText) d.findViewById(R.id.fileUrlText);
        updateLabel();


        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };


        ////spinner
        spinner = (Spinner) d.findViewById(R.id.category_spinner);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Quality");
        categories.add("Safety");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            if (contentUri != null && contentFileUri != null){
                uriArrayList.add(contentUri);
                uriArrayList.add(contentFileUri);
                uploadfile(uriArrayList);
                uploadDoc(contentFileUri);
            }
            else if(contentUri != null && contentFileUri == null){
                uriArrayList.add(contentUri);
                uploadfile(uriArrayList);
            }
            else if(contentFileUri != null && contentUri == null){
                uploadDoc(contentFileUri);
            }
            else{
                fire.saveOnline(issueEditText.getText().toString(),
                        urlEditText.getText().toString(),
                        dateEditText.getText().toString(),
                        itemSpinner,
                        fileUrl);
                issueEditText.setText("");
                urlEditText.setText("");
                dateEditText.setText("");
                d.dismiss();
            }
                //urlEditText.setText(downloadUrl.toString());

            }
        });

        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        fileUrlText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPickDoc();
            }
        });

        CameraimgView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
//                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                dispatchTakePictureIntent();
//                v.invalidate();

                //forceWrapContent(v);
            }
        });
        d.show();
    }

    private void updateLabel() {

        String myFormat = "d-MMM-yyyy"; //In which you need put here "EEE, d MMM yyyy HH:mm:ss Z"
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateEditText.setText(sdf.format(myCalendar.getTime()));
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.totalbp.issuemanagement.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
//            ImageView mImageView = (ImageView) findViewById(R.id.mImageView);
//            mImageView.setImageBitmap(imageBitmap);
//        }

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(mCurrentPhotoPath);
            contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
            CameraimgView = (ImageView) d.findViewById(mImageView);
            CameraimgView.setImageBitmap(decodeSampledBitmapFromFile(f.getAbsolutePath(), 500, 250));
            //getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        }

        if (requestCode == FilePickerConst.REQUEST_CODE_DOC ) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                docPaths = new ArrayList<>();
                docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                addThemToView(docPaths);
            }

        }





    }

    public void uploadToDatabase(){
        //urlString = downloadUrl.toString();
        urlEditText.setText(urlString);
        fire.saveOnline(issueEditText.getText().toString(),
                urlEditText.getText().toString(),
                dateEditText.getText().toString(),
                itemSpinner,fileUrl);
        issueEditText.setText("");
        urlEditText.setText("");
        d.dismiss();
    }


    public void  uploadfile(ArrayList<Uri> file){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading");
        progressDialog.show();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReferenceFromUrl("gs://researchapp-9ee34.appspot.com");
//
//        Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
        StorageReference riversRef = storageRef.child("images/"+file.get(0).getLastPathSegment());

        d.findViewById(mImageView).setDrawingCacheEnabled(true);
        d.findViewById(mImageView).buildDrawingCache();
        Bitmap bitmap =  d.findViewById(mImageView).getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = riversRef.putBytes(data);
//        UploadTask uploadTask = riversRef.putFile(file);

// Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                downloadUrl = taskSnapshot.getDownloadUrl();
                urlString = downloadUrl.toString();
                urlEditText.setText(urlString);
                if (contentFileUri == null){
                    fire.saveOnline(issueEditText.getText().toString(),
                            urlEditText.getText().toString(),
                            dateEditText.getText().toString(),
                            itemSpinner,
                            fileUrl
                    );
                    issueEditText.setText("");
                    urlEditText.setText("");
                    fileUrlText.setText("");
                    d.dismiss();
                }

            }
        });
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                if (isNetworkAvailable()){
                    //calculating progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    //displaying percentage in progress dialog
                    progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                }
                else{
                    progressDialog.dismiss();
                    d.dismiss();
                    Toast.makeText(getApplicationContext(), "Data will be upload later when connection is available", Toast.LENGTH_LONG).show();
                }


            }
        });
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //if the upload is successfull
                //hiding the progress dialog
                progressDialog.setMessage("Uploaded 100%...");
                progressDialog.dismiss();

                //and displaying a success toast
                Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
            }
        });
    }

    public static Bitmap decodeSampledBitmapFromFile(String path,
                                                     int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }

        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }


        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    protected void onPickDoc() {
        if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }
        else{
            PickDock();
        }

    }

    protected void PickDock(){
        FilePickerBuilder.getInstance().setMaxCount(1)
                .setSelectedFiles(docPaths)
                .setActivityTheme(R.style.AppTheme)
                .pickDocument(this);
    }

    protected void forceWrapContent(View v) {
        // Start with the provided view
        View current = v;

        // Travel up the tree until fail, modifying the LayoutParams
        do {
            // Get the parent
            ViewParent parent = current.getParent();

            // Check if the parent exists
            if (parent != null) {
                // Get the view
                try {
                    current = (View) parent;
                    ViewGroup.LayoutParams layoutParams = current.getLayoutParams();
                    if (layoutParams instanceof FrameLayout.LayoutParams) {
                        ((FrameLayout.LayoutParams) layoutParams).
                                gravity = Gravity.CENTER_HORIZONTAL;
                    } else if (layoutParams instanceof WindowManager.LayoutParams) {
                        ((WindowManager.LayoutParams) layoutParams).
                                gravity = Gravity.CENTER_HORIZONTAL;
                    }
                } catch (ClassCastException e) {
                    // This will happen when at the top view, it cannot be cast to a View
                    break;
                }

                // Modify the layout
                current.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        } while (current.getParent() != null);

        // Request a layout to be re-done
        current.requestLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PickDock();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void addThemToView(ArrayList<String> docPaths) {
        ArrayList<String> filePaths = new ArrayList<>();

        if(docPaths!=null)
            filePaths.addAll(docPaths);

        //Toast.makeText(this, "Num of files selected: "+ filePaths.size(), Toast.LENGTH_SHORT).show();

        File f = new File(filePaths.get(0));
        contentFileUri = Uri.fromFile(f);
        fileUrl = contentFileUri.toString();
        String fileName = fileUrl.substring( fileUrl.lastIndexOf('/')+1, fileUrl.length() );
        fileUrlText.setText(fileName);

        Toast.makeText(this, "Files selected: "+ fileName, Toast.LENGTH_SHORT).show();


        //Toast.makeText(this, "files selected: "+ fileName, Toast.LENGTH_SHORT).show();
        //uploadDoc(contentUri);
    }

    public void  uploadDoc(Uri file){
        final ProgressDialog progressDialogFile = new ProgressDialog(this);
        progressDialogFile.setTitle("Uploading Attachment");
        progressDialogFile.show();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReferenceFromUrl("gs://researchapp-9ee34.appspot.com");
        StorageReference riversRef = storageRef.child("docs/"+file.getLastPathSegment());
        UploadTask uploadTask = riversRef.putFile(file);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(getApplicationContext(), "Error: upload attachment", Toast.LENGTH_SHORT).show();
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                progressDialogFile.setMessage("Attachment Uploaded 100 %...");
                progressDialogFile.dismiss();


                fileDownloadUrl = taskSnapshot.getDownloadUrl();
                fileUrl = fileDownloadUrl.toString();
                fire.saveOnline(issueEditText.getText().toString(),
                        urlEditText.getText().toString(),
                        dateEditText.getText().toString(),
                        itemSpinner,
                        fileUrl
                );
                issueEditText.setText("");
                urlEditText.setText("");
                fileUrlText.setText("");
                d.dismiss();
                //and displaying a success toast
                Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                if (isNetworkAvailable()){
                    //calculating progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    //displaying percentage in progress dialog
                    progressDialogFile.setMessage("Attachment Uploaded " + ((int) progress) + "%...");
                }
                else{
                    progressDialogFile.dismiss();
                    d.dismiss();
                    Toast.makeText(getApplicationContext(), "Data will be upload later when connection is available", Toast.LENGTH_LONG).show();
                }


            }
        });
    }
}
