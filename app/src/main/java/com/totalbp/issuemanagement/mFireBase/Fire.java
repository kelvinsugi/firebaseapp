package com.totalbp.issuemanagement.mFireBase;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.ListView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.totalbp.issuemanagement.mData.Issue;
import com.totalbp.issuemanagement.mListView.CustomAdapter;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by 140030 on 01/02/2017.
 */

public class Fire {
    Context c;
    ArrayList<Issue> issues = new ArrayList<>();
    CustomAdapter adapter;
    ListView lv;
    Firebase firebase;
    String DB_URL;
    ProgressDialog progressDialog;
    public Fire(Context c, String DB_URL, ListView lv) {
        this.c = c;
        this.DB_URL = DB_URL;
        this.lv = lv;
        //INITIALIZE
        Firebase.setAndroidContext(c);
        firebase.getDefaultConfig().setPersistenceEnabled(true);
        firebase = new Firebase(DB_URL);


    }

    //save data
    public void saveOnline(String issue, String url, String issueDate, String issueCategory, String fileUrl){
        Issue i = new Issue();
        i.setIssueTxt(issue);
        i.setUrl(url);
        i.setIssueDate(issueDate);
        i.setCategory(issueCategory);
        i.setFileUrl(fileUrl);
        i.setTimeStamp(String.valueOf(-1 * new Date().getTime()));
        firebase.child("Issue").push().setValue(i);

    }

    public void refreshDataInOrder(){

    }


    //retrieveing
    public void refershData(){

//        firebase.addListenerForSingleValueEvent(new ValueEventListener() {
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                progressDialog.dismiss();
//                Log.d("END PROGRESS LOG",String.valueOf(dataSnapshot.getKey()));
//            }
//            public void onCancelled(FirebaseError firebaseError) { }
//        });

        firebase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                progressDialog = new ProgressDialog(lv.getContext());
//                progressDialog.setTitle("Loading");
//                progressDialog.show();
//                Log.d("START PROGRESS LOG",String.valueOf(dataSnapshot.getKey()));
                fetchUpdates(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Log.d("CHILD CHANGED LOG",String.valueOf(dataSnapshot.getKey()));
                fetchUpdates(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void fetchUpdates(DataSnapshot dataSnapshot){
        issues.clear();
        for(DataSnapshot ds : dataSnapshot.getChildren()){
            Issue i = new Issue();
            i.setIssueTxt(ds.getValue(Issue.class).getIssueTxt());
            i.setUrl(ds.getValue(Issue.class).getUrl());
            i.setIssueDate(ds.getValue(Issue.class).getIssueDate());
            i.setCategory(ds.getValue(Issue.class).getCategory());
            i.setFileUrl(ds.getValue(Issue.class).getFileUrl());
            i.setKey(ds.getKey());
//            Log.d("KEYS",String.valueOf( ds.getKey()));
            issues.add(i);
        }

        if(issues.size()>0){
            adapter = new CustomAdapter(c,issues);
            lv.setAdapter(adapter);
        }
    }


}
